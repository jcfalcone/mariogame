﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pauseScript : MonoBehaviour 
{
    GameObject canvas;

	// Use this for initialization
	void Start () 
    {
        this.canvas = GameObject.Find("Pause_Canvas");

        this.canvas.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetKeyUp(KeyCode.Escape) || Input.GetKeyUp(KeyCode.P))
        {
            GameManagerLevel1 instance = GameManagerLevel1.instance as GameManagerLevel1;

            Time.timeScale = (Time.timeScale == 0)? 1 : 0;
            instance.Pause = !instance.Pause;

            this.canvas.SetActive(!this.canvas.active);
        }
	}
}
