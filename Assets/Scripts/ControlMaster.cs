﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlMaster : MonoBehaviour 
{
    [SerializeField]
    GameObject[] spawnPoints;

    [SerializeField]
    GameObject[] collectiblesPrefabs;

	// Use this for initialization
	void Start () 
    {

        this.spawnPoints = GameObject.FindGameObjectsWithTag("Spawn");

        foreach(GameObject spawn in this.spawnPoints)
        {
            int numCollect = (Random.Range(0, 1000) % this.collectiblesPrefabs.Length);
            Instantiate(this.collectiblesPrefabs[numCollect], 
                        spawn.transform.position,
                        spawn.transform.rotation,
                        spawn.transform);
                            
        }
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
