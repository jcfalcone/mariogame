﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class addLifes : templateItem 
{
    [SerializeField]
    [Range(0,10)]
    int lifes;


    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.transform.tag == "Player" )
        {
            marioController player = col.transform.GetComponent<marioController>();

            player.lifes += this.lifes;

            base.removeObject();
        }
    }
}
