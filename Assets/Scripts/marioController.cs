﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class marioController : templateCharacter 
{

    [SerializeField]
    int _points;


    public int points
    {
        get{ return this._points; }
        set{ this._points = value; }
    }

    [SerializeField]
    int _lifes;

    public int lifes
    {
        get{ return this._lifes; }
        set
        {
            this._lifes = value;
            GameManagerLevel1 instance = GameManagerLevel1.instance as GameManagerLevel1;
            instance.updateLifes(this._lifes);
        }
    }


    [SerializeField]
    [Range(0f,1000f)]
    float jumpSpeed;

    [SerializeField]
    [Range(0f,1000f)]
    float walkSpeed;

    [SerializeField]
    [Range(0f,1000f)]
    float runSpeed;


    float speed;

    [SerializeField]
    bool canFloat = true;

    [SerializeField]
    bool isFloating;

    [SerializeField]
    [Range(0f,10f)]
    float floatTime;

    float originalGravity;
    float originalMass;

    [SerializeField]
    bool isGrounded;

    [SerializeField]
    [Range(0f,1f)]
    float groundCheckRadius;

    [SerializeField]
    LayerMask groundLayer;

    [SerializeField]
    GameObject groundCheck;

    IEnumerator stopFloating()
    {
        yield return new WaitForSeconds(this.floatTime);

        isFloating = false;
        canFloat = false;

        this.charRB.mass = this.originalMass;
    }

    protected override void Awake()
    {
        base.Awake();
        this.originalGravity = this.charRB.gravityScale;
        this.originalMass    = this.charRB.mass;
    }

	// Use this for initialization
	void Start () 
    {
        if (!this.groundCheck)
        {
            Debug.LogError("groundCheck not set for the marioController", gameObject);
            Debug.Break();
        }

        this.lifes = this.lifes;
	}

    public override void Die()
    {
        GameManagerLevel1 instance = GameManagerLevel1.instance as GameManagerLevel1;
        instance.gameOver();
    }
	
	// Update is called once per frame
	void Update () 
    {
        GameManagerLevel1 instance = GameManagerLevel1.instance as GameManagerLevel1;

        if (instance.Pause)
        {
            return;
        }

        //Move to the sides
        float hValue =  Input.GetAxis ("Horizontal"); //return a value between -1 and 1
        this.charRB.velocity = new Vector2(hValue * this.walkSpeed, this.charRB.velocity.y);

        this.charAnim.SetFloat("moveValue", hValue);

        this.charAnim.SetBool("floating", isFloating);

        this.charAnim.SetBool("crouching", false);

        if (isGrounded)
        {
            bool marioAttack = Input.GetButtonDown("Fire1");
            this.charAnim.SetBool("attacking", marioAttack);

            if (marioAttack)
            {
                fireBullet((Input.GetAxis("Horizontal") < 0));
            }
        }

        if (Input.GetAxis("Vertical") < 0)
        {
            this.charAnim.SetBool("crouching", true);
        }

        //Check if it's touching the ground and Jump if it's touching
        isGrounded = Physics2D.OverlapCircle(groundCheck.transform.position, groundCheckRadius, this.groundLayer);


        if (isGrounded && !isFloating)
        {
            canFloat = true;

            this.charAnim.SetBool("jumping", false);
            this.charAnim.SetBool("stomp", false);

            this.charRB.gravityScale = this.originalGravity;
        }

        if (!isGrounded && !isFloating)
        {
            this.charAnim.SetBool("jumping", true);

            if (this.charAnim.GetBool("stomp"))
            {
                this.charRB.gravityScale = 5f;
            }
        }

        if (!isGrounded && !isFloating && Input.GetKey(KeyCode.DownArrow))
        {
            this.charAnim.SetBool("stomp", true);
            this.charRB.gravityScale = 5f;
        }


        if (Input.GetButtonUp("Jump") && (isGrounded || isFloating) && canFloat)
        {
            this.charAnim.SetBool("jumping", true);
            canFloat = false;

            if (isFloating)
            {
                this.charRB.AddForce(Vector3.up * this.jumpSpeed * 0.1f);
            }
            else
            {
                this.charRB.AddForce(Vector3.up * this.jumpSpeed);
            }
        }

        /*if (Input.GetButtonUp("Jump") && Input.GetKey(KeyCode.UpArrow) && !isGrounded && !isFloating)
        {
        }*/
	}

    public void floatPlayer()
    {
        this.charRB.mass = 0.1f;

        this.charRB.AddForce(Vector3.up * this.jumpSpeed * 0.15f);

        canFloat = true;
        isFloating = true;

        StartCoroutine(stopFloating());
    }
}
