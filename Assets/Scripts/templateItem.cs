﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class templateItem : MonoBehaviour 
{
    [SerializeField]
    [Range(0,10)]
    int deathScore;

    protected void removeObject()
    {
        GameManagerLevel1 instance = GameManagerLevel1.instance as GameManagerLevel1;
        instance.Score += this.deathScore;

        Destroy(gameObject);
    }
}
