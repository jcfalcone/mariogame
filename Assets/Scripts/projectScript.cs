﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectScript : MonoBehaviour {

    [SerializeField]
    [Range(0f,10f)]
    float damage;

    string _parentTag;

    public string parentTag
    {
        get{ return _parentTag; }
        set{ _parentTag = value; }
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
    }

    void OnCollisionEnter2D (Collision2D col)
    {

        if (col.transform.tag != parentTag && col.transform.tag != "Ground")
        {
            if (col.gameObject.GetComponent<templateCharacter>())
            {
                col.gameObject.SendMessage("applyDamage", this.damage);
            }
            else
            {
                Destroy(col.gameObject);
            }
        }

        if (col.transform.tag != parentTag)
        {
            Destroy(gameObject);
        }
    }
}
