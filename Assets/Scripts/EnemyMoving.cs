﻿using System.Collections;
using UnityEngine;

public class EnemyMoving : templateCharacter
{
    [SerializeField]
    [Range(0f, 10f)]
    float speed;

    float side = -1;

    void flip()
    {
        side *= -1f;

        Vector3 scaleFactor = transform.localScale;

        scaleFactor.x *= -1; //scaleFactor.x = -scaleFactor.x; || same thing

        transform.localScale = scaleFactor;

    }
    // Update is called once per frame
    void Update () 
    {
        this.charRB.velocity = new Vector2(side*speed, this.charRB.velocity.y);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.transform.tag != "Ground")
        {
            flip();
            Debug.Log(col.gameObject.name, col.gameObject);
        }
    }
}