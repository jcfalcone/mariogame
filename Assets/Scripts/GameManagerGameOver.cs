﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;//loads the game + other

public class GameManagerGameOver : GameManagerTemplate 
{
	
	// Update is called once per frame
	void Update () 
    {
        //Click button to start next scene
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
	}
}
