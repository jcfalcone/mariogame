﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;//loads the game + other

public class GameManagerLevel1 : GameManagerTemplate 
{
    [SerializeField]
    GameObject levelSpawnPoint;

    IEnumerator spawnPlayer()
    {
        while (Application.isLoadingLevel) yield return null;

        GameManagerLevel1.instance.spawnPlayer(this.levelSpawnPoint);
    }

    public void gameOver()
    {
        SceneManager.LoadScene(2);
    }

    public void mainMenu()
    {
        SceneManager.LoadScene(0);
    }

	// Use this for initialization
	void Start () 
    {
        base.Start();

        StartCoroutine(spawnPlayer());
	}
}
