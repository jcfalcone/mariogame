﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;//loads the game + other
using UnityEngine.UI;

public class GameManagerTemplate : MonoBehaviour 
{

    //Provides acces to private "_instance"
    //Variable must be declared
    //Variables must be static because method is static

    public static GameManagerTemplate instance
    {
        get { return _instance; }//can also use just get;
        set { _instance = value; }//can also use just set;
    }

    int _Score;

    //declare variable 'score' and create get/set function for it
    public int Score
    {
        get
        {
            return _Score;
        }

        set
        {
            _Score = value;
            updateScore();
        }
    }

    public bool Pause
    {
        get;
        set;
    }

    Text scoreText;

    Text lifesText;

    GameObject _Player;

    public GameObject Player
    {
        get { return _Player; }
        private set { _Player = value; }
    }

    //Creates a class variable to keep track of GameManger
    static GameManagerTemplate _instance = null;

    //used to initiate player
    [SerializeField]
    protected GameObject playerPrefab;

    // Use this for initialization
    protected void Start () 
    {
        //check if GameManager instance already exists in Scene
        if(instance)
        {
            //GameManager exists,delete copy
            DestroyImmediate(gameObject);
        }
        else
        {
            //assign GameManager to variable "_instance"
            instance = this;
        }

        scoreText = GameObject.Find("Text_Score").GetComponent<Text>();
        lifesText = GameObject.Find("Text_Lifes").GetComponent<Text>();

        updateScore();
    }

    public void updateScore()
    {
        scoreText.text = "Score: " + this.Score.ToString("0000");
    }

    public void updateLifes(int lifes)
    {
        lifesText.text = lifes.ToString("00")+"x";
    }

    //Called to Spawn player
    public void spawnPlayer(GameObject spawnLocation)
    {
        //Find where player should be spawned
        Transform spawnPointTransform = spawnLocation.transform;

        //Instantiate (Create) player GameObject
        this.Player = Instantiate(playerPrefab, spawnPointTransform.position, spawnPointTransform.rotation) as GameObject;

        this.Player.name = "Player";

    }

    public void quitGame()
    {
        Application.Quit();
        Debug.Break();
    }
}
