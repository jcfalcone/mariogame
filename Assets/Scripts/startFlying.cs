﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class startFlying : templateItem 
{


    void OnTriggerEnter2D(Collider2D col)
    {
        
        if (col.transform.tag == "Player" )
        {
            marioController player = col.transform.GetComponent<marioController>();

            player.floatPlayer();

            base.removeObject();
        }
    }
}
