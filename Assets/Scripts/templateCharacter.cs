﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class templateCharacter : MonoBehaviour 
{

    protected Rigidbody2D charRB;
    protected Animator    charAnim;

    protected bool        canFireBullet = true;

    [SerializeField]
    [Range(0f,10f)]
    protected float life;

    [SerializeField]
    GameObject bulletPrefab;

    [SerializeField]
    GameObject bulletSpawn;

    [SerializeField]
    [Range(0f,10000f)]
    float bulletForce;

    [SerializeField]
    [Range(0f,10f)]
    float bulletDestroySeconds;

    [SerializeField]
    [Range(0f,10f)]
    float bulletCooldown;

    [SerializeField]
    [Range(0,10)]
    int deathScore;

    protected void fireBullet(bool direction)
    {
        if (!canFireBullet)
        {
            return;
        }

        GameObject bullet = Instantiate(this.bulletPrefab, 
            this.bulletSpawn.transform.position, 
            this.bulletSpawn.transform.rotation);


        Vector2 characterSide =  direction? Vector2.left : Vector2.right;

        bullet.GetComponent<Rigidbody2D>().AddForce(characterSide * this.bulletForce);


        bullet.GetComponent<projectScript>().parentTag = transform.tag;

        Destroy(bullet, this.bulletDestroySeconds);

        canFireBullet = false;

        StartCoroutine(coolDownBullet());

    }

    public static float AngleDir(Vector2 A, Vector2 B)
    {
        return -A.x * B.y + A.y * B.x;
    }

    public virtual void Die()
    {
        GameManagerLevel1 instance = GameManagerLevel1.instance as GameManagerLevel1;
        instance.Score += this.deathScore;

        Destroy(gameObject);
    }

    virtual public void applyDamage(float damage)
    {
        life -= damage;

        if (life <= 0)
        {
            Die();
        }
    }


    IEnumerator coolDownBullet()
    {
        yield return new WaitForSeconds(this.bulletCooldown);

        canFireBullet = true;
    }



    protected virtual void Awake()
    {
        this.charRB   = gameObject.GetComponent<Rigidbody2D>();
        this.charAnim = gameObject.GetComponent<Animator>();
    }

	// Use this for initialization
	void Start () 
    {
        if (!this.bulletPrefab)
        {
            Debug.LogError("bulletPrefab not set for the "+gameObject.name, gameObject);
            Debug.Break();
        }	
	}
	
	// Update is called once per frame
	void Update () 
    {
	}
}
