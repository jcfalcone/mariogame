﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyCharacter : templateCharacter 
{
    GameObject player; 

    [SerializeField]
    GameObject sprite;

    [SerializeField]
    [Range(0f,10f)]
    float fireDistance;
        
	// Use this for initialization
	void Start () 
    {	
	}
	
	// Update is called once per frame
	void Update () 
    {
        GameManagerLevel1 instance = GameManagerLevel1.instance as GameManagerLevel1;

        if (instance.Pause)
        {
            return;
        }

        bool direction = true;

        if (player)
        {
            Vector3 relativePoint = transform.InverseTransformPoint(player.transform.position);
            float distancePlayer = Vector3.Distance(transform.position, player.transform.position);

            if (relativePoint.x > 0.0f)
            {
                sprite.transform.rotation = Quaternion.Euler(new Vector3(sprite.transform.eulerAngles.x, -180, sprite.transform.eulerAngles.z));
                direction = false;
            }
            else if (relativePoint.x < 0.0f)
            {
                sprite.transform.rotation = Quaternion.Euler(new Vector3(sprite.transform.eulerAngles.x, 0, sprite.transform.eulerAngles.z));
                direction = true;
            }

            if (distancePlayer < fireDistance)
            {
                fireBullet(direction);
            }
        }
        else
        {
            player = GameManagerLevel1.instance.Player;
        }
	}
}
