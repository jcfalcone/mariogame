﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;//loads the game + other

public class GameManagerTitle : GameManagerTemplate 
{
    // Update is called once per frame
    void Update () 
    {
        //Click button to start next scene
        if(Input.GetKeyDown(KeyCode.Return))
        {
            SceneManager.LoadScene(1);
        }

    }
}
