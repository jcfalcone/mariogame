﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class enemyObstacle : templateCharacter 
{
    [SerializeField]
    RectTransform healthBar;
    float healthScale;

	// Use this for initialization
	void Start () 
    {
        this.healthScale = healthBar.sizeDelta.x / this.life;
	}

    override public void applyDamage(float damage)
    {
        base.applyDamage(damage);

        Debug.Log(healthBar.sizeDelta);

        healthBar.sizeDelta = new Vector2(this.life * this.healthScale, healthBar.sizeDelta.y);
    }
}
